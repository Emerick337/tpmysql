﻿
emplog=/var/log/sauvegarde.log
empsauv=$HOME/sauvegarde



tdate=$(date +%Y%m%d-%H%M%S)
fsauv=$empsauv/sauvegrde$tdate.sql

log() {
	echo $tdate $1 >>$emplog
}

testemp() {
if [ ! -d $empsauv ]; then
mkdir $empsauv
fi
}

suppvers() {
find /empsauv/* -atime 7 -exec echo $tdate 'suppression de '{} >> $emplog \; -exec rm {} \;
}

testerrBDD() {
coder=$?
if [ "$coder" = 0]; then
log "sauvegarde $1 reussie"
else
log "erreur de sauvegarde sur $1 - Code : $coder"
fi
}

sauvbdd() {
	mysqldump --user=$1 --password=$2 --databases $3 > $fsauv
	echo -e "user= $1\npassword= $2\nemplacementsauvegarde= $empsauv\ndatabases : " > $HOME/.bachupconf
	log 'sauvegarde reussie'
}

sauvbdd2() {
user=$1
pw=$2
echo -e "user= $1\npassword= $2\nemplacementsauvegarde= $empsauv\ndatabases : " > $HOME/.backupconf
#shift 2 permet de supprimer les "2" premiers arguments, et de descendre de 2 les arguments suivants (l'argument 3 passe en argument 1, le 4 en 2, etc ...)
shift 2
	for i in "$@"; do
	mysqldump --user=$user--password=$pw --databases $i > $fsauv
	testerrBDD $i
	echo $i >> $HOME/.bachupconf
	done
}
